# Time Application

Time application consists of the frontend and backend parts
Frontend is written with help of the Vue.js framework
Backend is written using Node.js and Express
Database is MySQL

Environment variables store in .env file. To use this application create your own .env, using this:
> MYSQL_DB=your_db
> MYSQL_HOST=your_host
> MYSQL_PORT=your_port
> MYSQL_USER=your_username
> MYSQL_PASSWORD=your_password

