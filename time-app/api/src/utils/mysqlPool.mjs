import mysql from 'mysql2'


const MYSQL_HOSTNAME = String(process.env.MYSQL_HOSTNAME);
const MYSQL_PORTNAME = String(process.env.MYSQL_PORTNAME);
const MYSQL_DB_NAME = String(process.env.MYSQL_DB_NAME);
const MYSQL_USERNAME = String(process.env.MYSQL_USERNAME);
const MYSQL_USER_PASSWORD = String(process.env.MYSQL_USER_PASSWORD);


const pool = mysql.createPool({
  connectionLimit: 100,
  host: MYSQL_HOSTNAME,
  port: MYSQL_PORTNAME,
  user: MYSQL_USERNAME,
  password: MYSQL_USER_PASSWORD,
  database: MYSQL_DB_NAME,
})

console.log(process.env)
console.log(pool)

const CREATE_TIMES_TABLE_SQL = `CREATE TABLE IF NOT EXISTS times (
  id INT AUTO_INCREMENT PRIMARY KEY,
  time TEXT,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)`

pool.getConnection((err, connection) => {
  if (!err) {
    console.log('Connected to the MySQL DB - ID is ' + connection.threadId)
    const createTimeTable = CREATE_TIMES_TABLE_SQL
    connection.query(createTimeTable, (err) => {
      if (!err) {
        console.log('Times table was created')
      }
    })
    connection.release()
  }
})

export default pool
